<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    // Ciclo for simples
    echo "Enquanto o número for menor ou igual a dez, o ciclo while simples é executado e o número incrementado<br><br>";

    for ($numero = 0; $numero <= 10; $numero++) {
        echo "$numero<br>";
    }

    // Ciclo foreach
    echo "<br>Considerando um array, que contenha diversos elementos, se aplicado o ciclo foreach a este array, será
    possível efectuar operações sobre cada um desses elementos individualmente<br><br>";

    echo 'Consideremos este array("verde", "azul", "vermelho", "amarelo", "cor-de-laranja")<br><br>';

    $cores = array("verde", "azul", "vermelho", "amarelo", "cor-de-laranja"); // Declarando o array

    /*
    Por cada elemento como $cor no array $cores
    A variável $cor existe localmente, sendo reescrita a cada execução do ciclo, assumindo sempre o valor do próximo
    elemento do array
    */
    foreach ($cores as $cor) {
        echo "A palavra $cor tem " . strlen($cor) . " caracteres<br>";
    }

    echo "<br>O ciclo termina quando o array não contém mais elementos<br><br>";
    ?>
</p>
</body>
</html>