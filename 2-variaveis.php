<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    $variavel_1 = "São como contentores para guardar dados<br><br>";

    $variavel_2 = "Começam com o símbolo $ ao qual imediatamente a seguir se coloca o nome<br><br>";

    $variavel_3 = "Têm que começar com uma letra ou um underscore não podem começar com números, e só podem ser
    compostas por caracteres alfanuméricos e underscores<br><br>";

    $variavel_4 = "Os nomes são case-sensitive<br><br>";

    $variavel_5 = "Na linguagem PHP não é necessário indicar o tipo de dados contidos nas variáveis<br><br>";

    echo $variavel_1, $variavel_2, $variavel_3, $variavel_4, $variavel_5;

    $a = 2;
    $b = 3;
    $c = $a - $b;

    echo "O valor da variável c é $c<br><br>";

    // Âmbito das variáveis
    $x = 5; // âmbito global

    function semEvocarGlobal()
    {
        $y = 10; // âmbito local
        echo "Dentro da função a variável x é: $x<br>";
        echo "Dentro da função a variável y é: $y<br>";
    }

    semEvocarGlobal(); // aqui o $x não está definido

    echo "Fora da função a variável x é: $x<br>";
    echo "Fora da função a variável y é: $y<br>"; // aqui o $y não está definido

    function evocarGlobal()
    {
        global $x; // para evocar a varíavel global
        $y = 10; // âmbito local
        echo "Dentro de uma função em que se evoca explicitamente a variável x como global a sua soma com a variável y
        é ", $x + $y;
    }

    evocarGlobal(); // 15
    ?>
</p>
</body>
</html>
