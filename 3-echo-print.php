<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    $a = print "Existem duas maneiras basicas de mostrar outputs, com as funções echo ou com print<br><br>";
    echo "O echo suporta mais que uma string.", " Enquanto o print apenas suporta uma e retorna sempre $a<br><br>";
    $array = array("verde", "azul", "amarelo");
    // utilizando plicas ao invés de aspas não acontece interpretação de variáveis
    echo 'echo A minha cor favorita é o {$array[0]}<br><br>';
    echo "A minha cor favorita é o {$array[0]}<br><br>";
    ?>
</p>
</body>
</html>
