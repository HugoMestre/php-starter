<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    // Condição If
    if(true){
        echo "O código dentro de um if é executado quanto a condição verificada neste é verdadeira<br><br>";
    }

    // Condição If Else
    $numero = rand(1,2); // Um número inteiro aleatório entre 1 e 2

    $resultado = $numero%2; // Obtendo o resto de divisão inteira por 2

    if($resultado == 0){
        echo "O número aleatório foi o 2<br><br>"; // Número Par
    }else{
        echo "O número aleatório foi o 1<br><br>"; // Número Ímpar
    }

    // Condição If ElseIf Else
    $numero = rand(0,3); // Um número inteiro aleatório entre 0 e 3

    if($numero == 3){
        echo "O número aleatório foi o 3<br><br>";
    }elseif($numero == 2){
        echo "O número aleatório foi o 2<br><br>";
    }elseif($numero == 1){
        echo "O número aleatório foi o 1<br><br>";
    }else{
        echo "O número aleatório foi o 0<br><br>";
    }

    // Switch, executa a condição que cumpre os requisitos
    $numero = rand(0,5); // Um número inteiro aleatório entre 0 e 5

    switch($numero){
        case 1:
            echo "Esta operação foi despoletada pelo número 1";
            break; // Os breaks são importantes num switch porque forçam a saída do mesmo
            echo 'Se o break não estivesse aqui, isto ia ser "imprimido" caso o número fosse o 1 e todas as outras
            condições abaixo definidas iam ser verificadas, isso é um transtorno que causa desperdício de recursos em
            termos de processamento, e noutros casos ainda como no bloco default pode levar a erros, visto que o número
            em questão teria sido o 1 e não o 0 nem o 5';
        case 2:
            echo "Esta operação foi despoletada pelo número 2";
            break;
        case 3:
            echo "Esta operação foi despoletada pelo número 3";
            break;
        case 4:
            echo "Esta operação foi despoletada pelo número 4";
            break;
        default:
            echo "Esta operação foi despoletada pelo número 0 ou 5";
    }
    ?>
</p>
</body>
</html>