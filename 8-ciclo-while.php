<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    // Ciclo while simples
    $numero = 0;

    echo "Enquanto o número for menor que três, o ciclo while simples é executado e o número incrementado<br><br>";

    while($numero < 3){
        ++$numero;
        echo "$numero<br>";
    }

    // Ciclo do while
    echo "<br>O ciclo do while irá ser executado segundo a mesma condição, mas usando o 3 obtido em cima<br><br>";

    do{
        ++$numero;
        echo "$numero<br>";
    }while($numero < 3);

    echo "<br>Portanto apesar da condição do ciclo não se verificar uma única vez, este foi executado uma vez<br><br>";


    ?>
</p>
</body>
</html>