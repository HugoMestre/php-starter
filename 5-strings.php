<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    $string = "O comprimento desta string pode ser avaliado usando a função strlen()";
    echo "$string<br><br>";
    echo strlen($string)."<br><br>";
    echo "A string pode ser encriptada. crypt(".'$string'.")<br><br>" . crypt($string) . "<br><br>";
    echo "Podem ser procuradas e substituidas partes da string.<br><br>";
    echo str_replace ('i' , 'w' , $string);
    ?>
</p>
</body>
</html>