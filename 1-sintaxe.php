<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    // Isto é um comentário, o código php é feito entre estas duas tags
    ?>
</p>
<p>
    <?php
    # Isto é um comentário também

    /*
    Isto é um comentário multi-linha.
    Os comentários servem para facilitar a leitura e percepção
    de código ou documentar o mesmo.
    */

    echo "As funções, classes e palavras reservadas, tais como if, else, while, entre outras, não são case-sensitive";
    eChO "<br><br>";

    $a = 'a';
    $A = 'A';

    ECHO "Já por sua vez as variáveis têm em conta a capitalização das letras, $a, $A";
    ?>
</p>
</body>
</html>
