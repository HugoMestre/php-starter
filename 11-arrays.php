<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    // Definição de array indexado
    echo 'Consideremos este array indexado array("zero", "um", "dois")<br><br>';
    $arrayIndexado = array("zero", "um", "dois");

    $tamanhoDoArrayIndexado = count($arrayIndexado); // retorna o número de elementos do array

    echo "O array tem $tamanhoDoArrayIndexado elementos<br><br>";

    for($i = 0; $i < $tamanhoDoArrayIndexado; $i++){
        echo "Posição {$arrayIndexado[$i]} do array indexado<br>";
    }

    // Definição de array associativo
    echo '<br>Consideremos agora um array associativo, que por norma serve para armazenar dados com lógica associada, neste
    caso a descrição de um cão<br><br>';

    $cao = array("Raça" => "Boxer", "Pelo" => "Castanho", "Patas" => "Quatro na sua grande maioria");

    foreach($cao as $key => $value){
        echo "$key - $value<br>";
    }

    echo '<br>Este foi o array utilizado array("Raça" => "Boxer", "Pelo" => "Castanho",
    "Patas" => "Quatro na sua grande maioria")<br><br>';

    echo "E sim na key Patas o valor realmente era {$cao['Patas']}";
    ?>
</p>
</body>
</html>