<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    // Operadores aritméticos
    $x=10;
    $y=6;
    echo ($x + $y)."<br><br>"; // outputs 16
    echo ($x - $y)."<br><br>"; // outputs 4
    echo ($x * $y)."<br><br>"; // outputs 60
    echo ($x / $y)."<br><br>"; // outputs 1.6666666666667
    echo ($x % $y)."<br><br>"; // outputs 4


    // Operadores de atribuição
    $x=10;
    echo $x."<br><br>"; // outputs 10
    $y=20;
    $y += 100; // Soma por atribuição
    echo $y."<br><br>"; // outputs 120
    $z=50;
    $z -= 25; // Subtração por atribuição
    echo $z."<br><br>"; // outputs 25
    $i=5;
    $i *= 6; // Multiplicação por atribuição
    echo $i."<br><br>"; // outputs 30
    $j=10;
    $j /= 5; // Divisão por atribuição
    echo $j."<br><br>"; // outputs 2
    $k=15;
    $k %= 4; // Resto de divisão inteira por atribuição
    echo $k."<br><br>"; // outputs 3


    // Operadores de strings
    $a = "Hello";
    $b = $a . " world!"; // concatenação
    echo $b."<br><br>"; // outputs Hello world!

    $x="Hello";
    $x .= " world!"; // concatenação por atribuição
    echo $x."<br><br>"; // outputs Hello world!


    // Operadores para Incrementar e Decrementar
    $x=10;
    echo ++$x."<br><br>"; // outputs 11  // Pré-incremento
    $y=10;
    echo $y++."<br><br>"; // outputs 10 // Pós-incremento
    $z=5;
    echo --$z."<br><br>"; // outputs 4 // Pré-decremento
    $i=5;
    echo $i--."<br><br>"; // outputs 5 // Pós-decremento


    // Operadores de Comparação
    $x=100; // Tipo Integer
    $y="100"; // Tipo String
    var_dump($x == $y); echo "<br>";
    var_dump($x === $y); echo "<br>";
    var_dump($x != $y); echo "<br>";
    var_dump($x !== $y); echo "<br>";
    $a=50;
    $b=90;
    var_dump($a > $b); echo "<br>";
    var_dump($a < $b); echo "<br>";

    // Operadores Lógicos
    $x=100;
    $y="100";
    var_dump($x == $y && $x === $y); // Operador And
    echo "<br>";
    var_dump($x != $y || $x !== $y); // Operador Or
    echo "<br>";
    $a=50;
    $b=90;
    var_dump(!($a > $b)); // Operador de Negação
    echo "<br>";
    var_dump(!($a < $b)); // Operador de Negação
    $a = 100;
    $a = !$a;
    echo "<br>";
    var_dump($a);
    ?>
</p>
</body>
</html>