<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php

    echo "A linguagem PHP tem diversos tipos de dados. Nomeadamente String, Integer, Float, Boolean, Array, Object,
    NULL.<br><br>";
    $a = "Eu sou uma string";
    $b = 1;
    $c = 2.1;
    $d = false;
    $e = new stdClass(); // Classe vazia e genérica da linguagem PHP;
    $f = null;
    $g = array($a, $b, $c, $d, $e, $f);

    foreach ($g as $item) { // O ciclo foreach será explicado posteriormente
        var_dump($item); // A função var_dump imprime o tipo e o valor de uma expressão de uma forma estruturada
        echo "<br><br>";
    }
    var_dump($g);
    ?>
</p>
</body>
</html>
