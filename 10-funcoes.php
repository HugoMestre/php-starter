<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<p>
    <?php
    // Definição de função simples
    function simples(){
        echo "Esta é uma função simples, sem argumentos nem retorno<br><br>";
    }

    // Definição de função com retorno
    function comRetornoSemArgumentos(){
        return "Esta é uma função com retorno, sem argumentos<br><br>";
    }

    // Definição de função com retorno com argumentos
    function comRetornoComArgumentos($umArgumento){
        return "Esta é uma função com retorno e com um argumento de valor $umArgumento<br><br>";
    }

    // Definição de função sem retorno com argumentos com valores por defeito
    function semRetornoComArgumentosComValoresPorDefeito($umArgumento = 'qualquer coisa'){
        echo "Esta é uma função sem retorno e com argumento com valor por defeito se não fornecido<br>";
        echo "Vai ser $umArgumento<br><br>";
    }

    simples();

    $retorno = comRetornoSemArgumentos(); // Captura o retorno
    echo $retorno;

    $retorno = comRetornoComArgumentos('O que me apetecer');
    echo $retorno;
    $retorno = comRetornoComArgumentos(6);
    echo $retorno;

    semRetornoComArgumentosComValoresPorDefeito(); // Sem fornecer valor para o argumento
    semRetornoComArgumentosComValoresPorDefeito('um pastel de nata'); // Fornecendo valor para o argumento
    ?>
</p>
</body>
</html>